from django.contrib import admin

from .models import Entree_journal, Competence, Techno, Etape

# Register your models here.

admin.site.register(Entree_journal)
admin.site.register(Competence)
admin.site.register(Techno)
admin.site.register(Etape)