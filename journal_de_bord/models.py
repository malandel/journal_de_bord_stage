from django.db import models
from datetime import datetime

# Create your models here.


class Competence(models.Model):
    COMPETENCE_TYPE_CHOICES = [ 
        ('FRONT', 'Front-end'), 
        ('BACK', 'Back-end')
    ]
    COMPETENCE_DETAIL_CHOICES = [
        ('SEPARATOR', '--FRONT-END--'), 
        ('Maquetter une application', 'Maquetter une application'), 
        ('Réaliser une interface web statique et adaptable', 'Réaliser une interface web statique et adaptable'),
        ('Développer une interface web dynamique', 'Développer une interface web dynamique'),
        ('CMS FRONT', 'Réaliser une interface utilisateur avec une solution de gestion de contenu ou e-commerce'),
        ('SEPARATOR', '--BACK-END--'), 
        ('Créer une base de données', 'Créer une base de données'),
        ('Développer les composants d\'accès aux données', 'Développer les composants d\'accès aux données'),
        ('Développer la partie back-end d’une application web ou web mobile', 'Développer la partie back-end d’une application web ou web mobile'),
        ('CMS BACK', 'Elaborer et mettre en œuvre des composants dans une application de gestion de contenu ou e-commerce')
    ]
    competence_type = models.CharField(max_length = 200, choices = COMPETENCE_TYPE_CHOICES)
    competence_detail = models.CharField(max_length = 200, choices = COMPETENCE_DETAIL_CHOICES) 
    commentaires = models.TextField()
    
    def __str__(self):
        return self.competence_detail

class Techno(models.Model):
    nom = models.CharField(max_length=200)

    def __str__(self):
        return self.nom

class Etape(models.Model):
    nom = models.CharField(max_length=200)

    def __str__(self):
        return self.nom

class Entree_journal(models.Model):
    numero_semaine = models.IntegerField()
    numero_jour = models.IntegerField(default=0)
    date = models.DateField()
    a_faire = models.TextField(null=True, blank = True)
    fait = models.TextField(null=True, blank = True)
    problemes_rencontres = models.TextField(null=True, blank = True)
    resolution_problemes = models.TextField(null=True, blank = True)
    ressources_utilisees = models.TextField(null=True, blank = True)
    poursuite = models.TextField(null=True, blank = True)
    autre = models.TextField(null=True, blank = True)
    competences = models.ManyToManyField(Competence, blank=True)
    technos = models.ManyToManyField(Techno, blank=True)
    etape = models.ManyToManyField(Etape, blank=True)

    def __str__(self):
        str_date = self.date.strftime("%d-%m-%Y")
        sortie = str_date + " - Jour " + str(self.numero_jour)
        return sortie
