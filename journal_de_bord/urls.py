from django.urls import path

from . import views
# Créé un espace de nom 'app' pour compartimenter les urls (avec les noms donnés à chacune)
# Evite les conflits si plusieurs apps avec mêmes noms d'url
app_name = 'jdb'

urlpatterns = [
    # ex : /
    path('', views.home, name='home')
]